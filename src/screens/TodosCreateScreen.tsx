import {
  TextInput,
  Button,
  Switch,
  Text,
  PaperProvider,
  HelperText,
} from 'react-native-paper';
import { View } from 'react-native';
import { useForm, Controller, type SubmitHandler } from 'react-hook-form';
import { useTodosStore } from '@store/index';
import { useNavigation } from '@react-navigation/native';
import { zodResolver } from '@hookform/resolvers/zod';

import { useEffect, useState } from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import dayjs from 'dayjs';

import { TodoSchema } from '../types/todo';
export const TodosCreateScreen = ({ route }: TodosCreateScreenTypes) => {
  const [mode, setMode] = useState<'date' | 'time'>('date');
  const [show, setShow] = useState(false);
  const showMode = (currentMode: 'date' | 'time') => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };
  const { actions } = useTodosStore();
  const { goBack } = useNavigation();

  const { handleSubmit, control, reset, watch } = useForm<Todo>({
    resolver: zodResolver(TodoSchema),
    defaultValues: {
      deadlineDate: dayjs().add(1, 'day').startOf('day').toDate(),
    },
  });

  const onSubmit: SubmitHandler<Todo> = (date) => {
    if (!route.params) {
      actions.createTodos(date);
    } else {
      actions.updateTodos({
        id: route.params.id,
        ...date,
        completed: false,
      });
    }
    goBack();
  };

  useEffect(() => {
    if (route.params) {
      const items = route.params;
      reset({
        ...items,
      });
    }
  }, [route.params]);

  return (
    <PaperProvider>
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 16,
          borderRadius: 3,
        }}
      >
        <Controller
          control={control}
          render={({
            field: { onChange, onBlur, value },
            fieldState: { error },
          }) => (
            <View style={{ margin: 8, width: '100%' }}>
              <TextInput
                onBlur={onBlur}
                onChangeText={(v) => onChange(v)}
                value={value}
                mode='outlined'
                label='Title'
                error={!!error}
              />
              <HelperText type='error' visible={!!error}>
                {error?.message}
              </HelperText>
            </View>
          )}
          name='title'
          rules={{
            required: {
              value: true,
              message: 'Email is required',
            },
          }}
        />
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              onBlur={onBlur}
              onChangeText={(v) => onChange(v)}
              value={value}
              style={{
                margin: 8,
                width: '100%',
                minHeight: 100,
                maxHeight: 200,
                borderRadius: 3,
              }}
              mode='outlined'
              multiline
              label='Description'
            />
          )}
          name='description'
          rules={{ required: false }}
        />

        <Controller
          control={control}
          render={({ field: { onChange, value } }) => (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                gap: 10,
                width: '100%',
                marginTop: 16,
                marginBottom: 16,
              }}
            >
              <Text>Deadline:</Text>
              <Switch value={value} onValueChange={onChange} />
            </View>
          )}
          name='deadline'
        />

        {watch('deadline') && (
          <Controller
            control={control}
            render={({ field: { onChange, value } }) => (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  gap: 10,
                  width: '100%',
                  marginTop: 16,
                  marginBottom: 16,
                }}
              >
                <Button
                  onPress={showDatepicker}
                  mode={'outlined'}
                  style={{
                    width: '50%',
                  }}
                >
                  Date: {dayjs(value).format('DD.MM.YYYY')}
                </Button>
                <Button
                  onPress={showTimepicker}
                  mode={'outlined'}
                  style={{
                    width: '50%',
                  }}
                >
                  Time: {dayjs(value).format('HH:mm')}
                </Button>

                {show && (
                  <DateTimePicker
                    testID='dateTimePicker'
                    value={dayjs(value).toDate()}
                    mode={mode}
                    is24Hour={true}
                    onChange={(_, date) => {
                      setShow(false);
                      onChange(date);
                    }}
                  />
                )}
              </View>
            )}
            name='deadlineDate'
            rules={{
              required: {
                value: true,
                message: 'Email is required',
              },
            }}
          />
        )}

        <Button
          style={{
            width: '100%',
            borderRadius: 3,
          }}
          mode='contained'
          onPress={handleSubmit(onSubmit)}
        >
          Add Todo
        </Button>
      </View>
    </PaperProvider>
  );
};
