import { PaperProvider } from 'react-native-paper';
import { View, ScrollView, Text } from 'react-native';

import { useTodosStore } from '@store/index';
import { TodosCard } from '@components/Todos/TodosCard';


export const TodosScreen = () => {
  const { todos } = useTodosStore();
  return (
    <PaperProvider>
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 16,
          borderRadius: 3,
        }}
      >
        <ScrollView
          style={{
            width: '100%',
            height: '100%',
          }}
        >
          <View
            style={{
              width: '100%',
              height: '100%',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              gap: 10,
            }}
          >
            {todos.map((i) => (
              <TodosCard
                key={i.id}
                id={i.id}
                title={i.title}
                description={i.description}
                completed={i.completed}
                deadline={i.deadline}
                deadlineDate={i.deadlineDate}
              />
            ))}
          </View>
        </ScrollView>
      </View>
    </PaperProvider>
  );
};
