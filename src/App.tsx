import { registerRootComponent } from 'expo';
import { useEffect } from 'react';
import { useTodosStore } from '@store/index';

import { Routers } from '@components/Routers';
export default function App() {
  const { actions } = useTodosStore();
  useEffect(() => {
    actions.getTodos();
  }, [actions]);
  return <Routers />;
}

registerRootComponent(App);
