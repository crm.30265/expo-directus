import { type NativeStackScreenProps, type NavigationProp } from "@react-navigation/native-stack";
import { RootStackParamList } from "./navigation";

import { Todo } from "./todo";


declare global {
    type useNavigationProps = NavigationProp<RootStackParamList>
    type TodosScreenTypes = NativeStackScreenProps<RootStackParamList, 'TodosScreen'>;
    type TodosCreateScreenTypes = NativeStackScreenProps<RootStackParamList, 'TodosCreateScreen'>;
    namespace Navigation {
        type RootParamList = RootStackParamList & {}
    }
    namespace Todos {

    }
    namespace NavigationProp {

        type RootStackParamList = RootStackParamList & {}
    }

    type Todo = Todo & {}
}