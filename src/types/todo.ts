import * as z from 'zod';

export const TodoSchema = z.object({
    title: z.string({
        required_error: 'Title Is Required field',
    }),
    description: z.string().optional(),
    deadline: z.boolean().default(false),
    deadlineDate: z.date().default(new Date()),
});


export type Todo = & z.infer<typeof TodoSchema> & {
    id: number;
    completed: boolean;
    deadline: boolean;
    deadlineDate: string | undefined;
};