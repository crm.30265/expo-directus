import { Card, Text, Checkbox, FAB } from 'react-native-paper';
import { View } from 'react-native';
import { TodosDeleteDialog } from '@components/Dialogs/TodosDeleteDialog';

import { useTodosStore } from '@store/index';
import { useNavigation } from '@react-navigation/native';
import dayjs from 'dayjs';

export const TodosCard = ({
  id,
  title,
  description,
  completed,
  deadline,
  deadlineDate,
}: Todo) => {
  const { actions } = useTodosStore();
  const { navigate } = useNavigation<useNavigationProps>();

  return (
    <Card
      key={id}
      mode={'outlined'}
      style={{
        width: '100%',
      }}
    >
      <Card.Content>
        <Text variant='titleLarge'>{title}</Text>
        <Text variant='bodyMedium'>{description}</Text>
        {deadline && (
          <Text variant='bodyMedium'>
            Deadline: {dayjs(deadlineDate).format('DD.MM.YYYY HH:mm:ss')}
          </Text>
        )}

        <Checkbox
          status={completed ? 'checked' : 'unchecked'}
          onPress={() =>
            actions.updateTodos({
              id,
              title,
              description: description || '',
              deadline,
              deadlineDate,
              completed: !completed,
            })
          }
        />

        <View
          style={{
            position: 'absolute',
            top: 10,
            right: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            gap: 10,
          }}
        >
          <FAB
            icon={'square-edit-outline'}
            size={'small'}
            color='black'
            mode='flat'
            variant='primary'
            onPress={() =>
              navigate('TodosCreateScreen', {
                id,
                title,
                description: description || '',
                deadline,
                deadlineDate,
              })
            }
          />
          <TodosDeleteDialog
            title={title}
            action={() => actions.deleteTodos(id)}
          >
            <FAB
              icon={'delete'}
              size={'small'}
              color='red'
              mode='flat'
              variant='tertiary'
            />
          </TodosDeleteDialog>
        </View>
      </Card.Content>
    </Card>
  );
};
