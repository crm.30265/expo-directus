import { useState, ReactNode } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Button, Dialog, Portal, Text } from 'react-native-paper';
type TodosDeleteDialogProps = {
  children: ReactNode;
  title: string;
  action: () => void;
};

export const TodosDeleteDialog = ({
  children,
  title,
  action,
}: TodosDeleteDialogProps) => {
  const [visible, setVisible] = useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);
  return (
    <View>
      <TouchableOpacity onPress={showDialog}>{children}</TouchableOpacity>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Delete todo {title}</Dialog.Title>
          <Dialog.Content>
            <Text variant='bodyMedium'>
              Are you sure you want to delete this todo?
            </Text>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>Close</Button>
            <Button onPress={action}>Delete</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};
