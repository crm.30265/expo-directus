import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { TodosScreen } from '@screens/TodosScreen';
import { TodosCreateScreen } from '@screens/TodosCreateScreen';
import { IconButton } from 'react-native-paper';

const Stack = createNativeStackNavigator<Navigation.RootParamList>();

export const Routers = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='TodosScreen'>
        <Stack.Screen
          name='TodosScreen'
          component={TodosScreen}
          options={({ navigation }) => ({
            headerRight: () => (
              <IconButton
                icon='plus'
                mode='contained'
                onPress={() => navigation.navigate('TodosCreateScreen')}
              />
            ),
          })}
        />
        <Stack.Screen name='TodosCreateScreen' component={TodosCreateScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
