export const booleanHelper = (value: boolean | 'TRUE' | 'FALSE') => {
    if (value === true) {
        return 'TRUE';
    }
    if (value === false) {
        return 'FALSE';
    }
    if (value === 'TRUE') {
        return true;
    }
    if (value === 'FALSE') {
        return false;
    }
};
