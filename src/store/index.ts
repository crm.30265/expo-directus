import { create } from 'zustand';
import { SQLError, SQLTransaction, openDatabase } from 'expo-sqlite';
import { booleanHelper } from "@utils/booleanHelper";



type TodosStore = {
    todos: Todo[];
    actions: {
        createTodos: (todo: Omit<Todo, 'id' | 'completed'>) => void;
        deleteTodos: (id: number) => void;
        updateTodos: (todo: Todo) => void;
        getTodos: () => void;
        resetDb: () => void;

    };
};

const db = openDatabase('sql.db');
db.transaction(tx => {
    tx.executeSql(`
CREATE TABLE IF NOT EXISTS todos (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    description TEXT NULL,
    completed BOOLEAN,
    deadline BOOLEAN,
    deadlineDate TEXT NULL
)
`);
});
const errorDb = (_: SQLTransaction, error: SQLError) => {
    console.log(error.message)
    return true;
};



export const useTodosStore = create<TodosStore>((set, get) => ({
    todos: [],
    actions: {
        createTodos: async todo => {
            // Вставка нового запису
            db.transaction(tx => {
                tx.executeSql(
                    'INSERT INTO todos (title, description, completed, deadline, deadlineDate) VALUES (?, ?, ?, ?, ?)',
                    [todo.title, todo.description ?? null, "FALSE", booleanHelper(todo.deadline), todo?.deadlineDate?.toString() ?? null],
                    (_, result) => {
                        console.log('Запис вставлено успішно. ID: ' + result.insertId);
                    },
                    errorDb
                );
            });

            get().actions.getTodos();
        }
        ,
        deleteTodos: id => {
            db.transaction(tx => {
                tx.executeSql(
                    'DELETE FROM todos WHERE id = ?',
                    [id],
                    (_, result) => {
                        if (result.rowsAffected > 0) {
                            get().actions.getTodos();
                        } else {
                            console.log('Запис із вказаним ID не було знайдено.');
                        }
                    },
                    errorDb
                );
            })
        },
        updateTodos: (todo) => {
            console.log(todo, 'todos');

            db.transaction(tx => {
                tx.executeSql(
                    'UPDATE todos SET title = ?, description = ?, completed = ?, deadline = ?, deadlineDate = ? WHERE id = ?',
                    [todo.title, todo.description ?? null, booleanHelper(todo.completed), booleanHelper(todo.deadline), todo?.deadlineDate?.toString(), todo.id],
                    (_, result) => {
                        if (result.rowsAffected > 0) {
                            console.log(result);
                        } else {
                            console.log('Запис із вказаним ID не було знайдено.');
                        }
                    },
                    errorDb
                );
            });
            get().actions.getTodos();
        },
        getTodos: () => {
            db.transaction(tx => {
                tx.executeSql(
                    'SELECT * FROM todos',
                    [],
                    (_, { rows: { _array } }) => {
                        const todos = _array as Todo[];
                        set({
                            todos: todos.map(todo => ({
                                ...todo,
                                completed: booleanHelper(todo.completed),
                                deadline: booleanHelper(todo.deadline),
                                deadlineDate: todo.deadlineDate ? new Date(todo.deadlineDate) : null
                            })),
                        })
                    },
                    errorDb
                );
            });
        }
        ,
        resetDb: () => {
            // if need to clear db
            db.transaction(tx => {
                tx.executeSql(
                    'DROP TABLE IF EXISTS todos',
                    [],
                    () => {
                        console.log('Таблицю "todos" видалено успішно.');
                    }
                );
            });
        }
    },

}));
